import Vue from "vue";
import VueRouter from "vue-router";
import Customer from "../views/Customer.vue";
import Invoice from "../views/Invoice.vue";
import InvoiceItem from "../views/InvoiceItem.vue";
import Product from "../views/Product.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/customer",
    name: "Customer",
    component: Customer,
  },
  {
    path: "/customer/:customerid/invoice",
    name: "Invoice",
    component: Invoice,
    props: true,
  },
  {
    path: "/customer/:customerid/invoice/:invoiceid/invoice_item",
    name: "InvoiceItem",
    component: InvoiceItem,
    props: true,
  },
  {
    path: "/product",
    name: "Product",
    component: Product,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
