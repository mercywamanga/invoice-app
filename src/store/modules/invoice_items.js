import axios from "axios";
const state = {
  invoice_items: [],
};
const getters = {
  allInvoiceItems: (state) => state.invoice_items,
};

const mutations = {
  GET_ALL_INVOICEITEMS(state, invoice_items) {
    state.invoice_items = invoice_items;
  },
};
//{ commit }  const invoiceitems =

const actions = {
  async fetchAllInvoiceItems({ commit }, payload) {
    await axios
      .get(
        `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${payload.customerid}/invoice/${payload.invoiceid}/invoice_item`
      )
      //console.log(invoiceitems);
      .then((response) => {
        console.log(response);
        commit("GET_ALL_INVOICEITEMS", response.data);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
