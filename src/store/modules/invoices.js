import axios from "axios";
const state = {
  invoices: [],
};
const getters = {
  allInvoices: (state) => state.invoices,
};

const mutations = {
  GET_ALL_INVOICES(state, invoices) {
    state.invoices = invoices;
  },
  ADD_INVOICE(state, invoice) {
    state.invoices.push(invoice);
  },

  DELETE_INVOICE(state, id) {
    state.invoices = state.invoices.filter((invoice) => invoice.id !== id);
  },
};
const actions = {
  async fetchAllInvoices({ commit }, id) {
    await axios
      .get(
        `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${id}/invoice`
      )
      .then((response) => {
        //console.log(response);
        commit("GET_ALL_INVOICES", response.data);
      });
  },

  async addInvoice({ commit }, customerid) {
    const invoice = await axios.post(
      `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${customerid}/invoice`
    );
    commit("ADD_INVOICE", invoice);
  },

  async deleteInvoice({ commit }, payload) {
    const invoice = await axios.delete(
      `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${payload.customerid}/invoice/${payload.invoiceid}`
    );

    commit("DELETE_INVOICE", invoice);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
