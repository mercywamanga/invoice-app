import axios from "axios";
const state = {
  products: [],
};
const getters = {
  allProducts: (state) => state.products,
};

const mutations = {
  GET_ALL_PRODUCTS(state, products) {
    state.products = products;
  },
  ADD_PRODUCT(state, product) {
    state.products.push(product);
  },
  DELETE_PRODUCT(state, id) {
    state.products = state.products.filter((product) => product.id !== id);
  },
};
const actions = {
  async fetchAllProducts({ commit }) {
    const products = await axios.get(
      "https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/product"
    );
    commit("GET_ALL_PRODUCTS", products.data);
  },
  async addProduct(context, data) {
    const product = await axios.post(
      "https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/product",
      data
    );
    console.log(product);
    // commit("ADD_PRODUCT", product);
  },
  async deleteProduct({ commit }, id) {
    await axios.delete(
      `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/product/${id}`
    );

    commit("DELETE_PRODUCT", id);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
