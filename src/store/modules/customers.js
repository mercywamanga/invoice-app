import axios from "axios";
const state = {
  customers: [],
};
const getters = {
  allCustomers: (state) => state.customers,
};

const mutations = {
  GET_ALL_CUSTOMERS(state, customers) {
    state.customers = customers;
  },
  ADD_CUSTOMER(state, customer) {
    state.customers.push(customer);
  },
  DELETE_CUSTOMER(state, id) {
    state.customers = state.customers.filter((customer) => customer.id !== id);
  },
  UPDATE_CUSTOMER(state, updtCustomer) {
    const index = state.customers.findIndex(
      (customer) => customer.id === updtCustomer.id
    );
    if (index !== -1) {
      state.customers.splice(index, 1, updtCustomer);
    }
  },
};
const actions = {
  async fetchAllCustomers({ commit }) {
    const customers = await axios.get(
      "https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer"
    );
    commit("GET_ALL_CUSTOMERS", customers.data.items);
  },
  async addCustomer({ commit }, data) {
    const customer = await axios.post(
      "https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer",
      data
    );
    //console.log(customer);
    commit("ADD_CUSTOMER", customer);
  },
  async deleteCustomer({ commit }, id) {
    await axios.delete(
      `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${id}`
    );

    commit("DELETE_CUSTOMER", id);
  },
  async updateCustomer({ commit }, updtCustomer) {
    const customer = await axios.put(
      `https://617a7d18cb1efe001700fecb.mockapi.io/sp5/api/v1/customer/${updtCustomer.id}`,
      updtCustomer
    );

    console.log(customer);

    commit("UPDATE_CUSTOMER", customer);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
