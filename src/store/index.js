import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import customers from "./modules/customers.js";
import invoices from "./modules/invoices.js";
import invoice_items from "./modules/invoice_items.js";
import products from "./modules/products.js";

Vue.use(Vuex);

//Create store.
export default new Vuex.Store({
  modules: {
    customers,
    invoices,
    invoice_items,
    products,
  },
  plugins: [createPersistedState()],
});
