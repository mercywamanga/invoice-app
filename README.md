# invoice-app

Invoice App is a web based invoice management system that helps an Admin track invoices efficiently. 
- The Admin is able to create /edit /delete and view customers and invoices.
- The Admin also has the ability to add/delete products to an invoice.

## Getting started

1. Clone project using https://gitlab.com/mercywamanga/invoice-app.git.
2. Run npm install to install all dependencies.
3. Run npm serve.
4. Go to url and enter http://localhost:8080/customer to go to the dashboard. 


## How the application works.
 1. Go to url and enter http://localhost:8080/customer to go to the dashboard. 
 2. User can add,edit,delete and Customer.
    - To add customer , click the add customer button , enter required data and save data.
    - To edit customer , click the edit icon button labelled green , enter data for update   
      and save data.
    - To delete customer , click the delete icon button.
 3. User can view customer's invoice, click the eye icon action button which redirects you  
    to  page with the invoice data.
    - User can add,edit and delete an invoice.
 4. User can also view products atttached to an invoice by clicking the eye icon button in    
    the invoice table.
    - User can delete an invoice item record. 
 5. User can also view ,add and delete products
